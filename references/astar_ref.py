
# plan - Returns cost to take all boxes in the todo list to dropzone
#
# ----------------------------------------
# modify code below
# ----------------------------------------
def plan(warehouse, dropzone, todo):

    init = [0,0]
    goal = [0,0]
    cost = 0
    grid = [[0 for row in range(len(warehouse[0]))] for col in range(len(warehouse))]

    for j in range(len(warehouse)):
        for k in range(len(warehouse[0])):
            if (warehouse[j][k] != 0):
                grid[j][k] = 1

    for i in range(len(todo)):

        #set start position of robot ## Done !!
        init[0] = dropzone[0]
        init[1] = dropzone[1]

        #set goal position (search from the warehouse) ## Done!!
        for j in range(len(warehouse)):
            if (warehouse[j].count(todo[i]) == 1):
                goal[1] = warehouse[j].index(todo[i])
                goal[0] = j

    # Make a grid from the ware house 1 for boxes, 0 for target box ## Done !!

    grid[goal[0]][goal[1]] = 0
    grid[init[0]][init[1]] = 0

    #Calculate min cost to pick up the object
    cost += search(grid,init,goal)

    # Make the new grid (set the target box location to 0; remove from warehouse as well)
    warehouse[goal[0]][goal[1]] = 0

    # set initial position to the goal position
    init[0] = goal[0]
    init[1] = goal[1]

    #set goal position as the dropzone; make deep copies!!
    goal[0] = dropzone[0]
    goal[1] = dropzone[1]

    # recalculate the cost for delivering the box
    cost += search(grid,init,goal)

    return cost
#************* The search function which gives the cost of min path *********
def search(grid,init,goal):

    delta = [[-1, 0 ], # go up      #adding diagonal moves as well
        [ 0, -1], # go left
        [ 1, 0 ], # go down
        [ 0, 1 ], # go right
        [-1, -1], # go NW
        [ 1, 1 ], # go SE
        [ 1, -1], # go SW
        [-1, 1 ]] # go NE

    cost = [1,1,1,1,1.5,1.5,1.5,1.5]

    closed = [[0 for row in range(len(grid[0]))] for col in range(len(grid))]
    closed[init[0]][init[1]] = 1

    expand = [[-1 for row in range(len(grid[0]))] for col in range(len(grid))]
    action = [[-1 for row in range(len(grid[0]))] for col in range(len(grid))]

    #**************************************************************************
    hrz_wt = 1
    diag_wt = 1.5

    heuristic = [[0 for row in range(len(grid[0]))] for col in range(len(grid))]
    ##    for i in range(len(heuristic)):
    ##        for j in range(len(heuristic[0])):

    ##            heuristic[i][j] = ((goal[1]-j)*(goal[1]-j)) + ((goal[0]-i)*(goal[0]-i))    # Eucledian squared distance
    ##            #*********************************************************************#
    ##            a = max(abs(goal[0]-i),abs(goal[1]-j))
    ##            b = min(abs(goal[0]-i),abs(goal[1]-j))
    ##
    ##            heuristic[i][j] = (a-b * hrz_wt) + (b * diag_wt)    # weighted chebychef heuristic
        #*****************************************************************************************

    x = init[0]
    y = init[1]
    g = 0

    h = heuristic[x][y]
    f = g+h

    open = [[f, g, h, x, y]]

    found = False  # flag that is set when search is complete
    resign = False # flag set if we can't find expand
    count = 0

    while not found and not resign:
        if len(open) == 0:
            resign = True
            return "Fail"
        else:
            open.sort()
            open.reverse()
            next = open.pop()
            x = next[3]
            y = next[4]
            g = next[1]

            expand[x][y] = g

            if x == goal[0] and y == goal[1]:
                found = True

            else:
                #exppand winning element and add it to the new open list
                for i in range(len(delta)):
                    x2 = x + delta[i][0]
                    y2 = y + delta[i][1]
                    if x2 >= 0 and x2 < len(grid) and y2 >=0 and y2 < len(grid[0]):
                        if closed[x2][y2] == 0 and grid[x2][y2] == 0:
                            g2 = g + cost[i]
                            h2 = heuristic[x2][y2]
                            f2 = g2 + h2
                            open.append([f2, g2, h2, x2, y2])

                            closed[x2][y2] = 1
    ##    for i in range(len(expand)):
    ##        print expand[i]
    ##    return expand #Leave this line for grading purposes!
        print(expand)
        return(g)

warehouse = ['1#2',
             '.#.',
             '..@']
todo = ['1', '2']
#dp = DeliveryPlanner(warehouse, todo)
#dp.plan_delivery()
search(warehouse,[2,2], [0,0])