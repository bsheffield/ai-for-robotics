from math import *
from matrix import *
from robot import *
import random

def next_move(hunter_position, hunter_heading, target_measurement, max_distance, OTHER=None):
    """
    Predicts next move with the Extended Kalman Filter.
    Discrete-time predict and update equations from https://en.wikipedia.org/wiki/Extended_Kalman_filter
    :param hunter_position: position of hunter robot
    :param hunter_heading: heading of hunter robot
    :param target_measurement: most recently observed target position
    :param max_distance: max allowed dist for hunter robot
    :param OTHER: used to keep track of previous estimates
    :return:
    """

    OTHER = ekf(target_measurement, OTHER)
    est_target_pos = OTHER["history"][-1]

    turning = 0
    distance = 0

    num_turns_wait = OTHER["num_turns_wait"]
    if num_turns_wait > len(OTHER["history"]):
        num_turns_wait = 1

    #wait for 'num_turns_wait' until history is built.
    # important for when we miss the target and have to wait again
    if len(OTHER["history"]) < num_turns_wait:
        return turning, distance, OTHER

    #look at previous target pos and set track to it until we arrive
    historical_est_pos = OTHER["history"][num_turns_wait-1]
    turning = angle_trunc(get_heading(hunter_position, historical_est_pos) - hunter_heading)

    historical_distance = distance_between(hunter_position, historical_est_pos)

    #keep on track until historical position is achieved.
    if OTHER["waiting"] is False and historical_distance > max_distance:
        #print("heading to historical")
        return turning, max_distance, OTHER

    #once in place, we wait for it to arrive again.
    if historical_distance == 0:
        OTHER['waiting'] = True
        #print("Waiting")

    #once target comes into range, calculate heading then try to catch it.
    #if we can't catch it, use the target's history to guess where it may be
    #'num_turns_wait' in time and try to cut it off.
    if distance_between(hunter_position, est_target_pos) <= max_distance:
        #print("target in range")
        distance = distance_between(hunter_position, est_target_pos)
        turning = angle_trunc(get_heading(hunter_position, est_target_pos) - hunter_heading)
        OTHER["num_steps"] += 1
    else:
        #print("target not in range.  Head back to historical")
        distance = historical_distance
        turning = angle_trunc(get_heading(hunter_position, historical_est_pos) - hunter_heading)

    #reset history if hunter catch catch within 125 steps
    if OTHER["num_steps"] > 60:
        OTHER = None

    return turning, distance, OTHER

def ekf(target_measurement, OTHER):
    """
    Extended Kalman filter used to predict non-linear location of target.
    :param target_measurement:
    :param OTHER:
    :return:
    """

    dim = 5  # represents the 5d EKF
    cov = 1000.  # error covariance
    noise = .05*2.0  # default noise level

    # initialize estimates and continually update.
    if OTHER is None:
        OTHER = dict()
        OTHER["num_steps"] = 0
        OTHER["history"] = list()
        OTHER["num_turns_wait"] = 1
        OTHER['waiting'] = False
        # x is used to predict state estimate
        OTHER["x"] = matrix([[]])
        OTHER["x"].zero(dim, 1)
        # P is used to predict covariance estimate
        OTHER["P"] = matrix([
            [cov, 0, 0, 0, 0],
            [0, cov, 0, 0, 0],
            [0, 0, cov, 0, 0],
            [0, 0, 0, cov, 0],
            [0, 0, 0, 0, cov]
        ])

    # observation matrix
    H = matrix([[1., 0, 0, 0, 0], [0, 1., 0, 0, 0]])

    # covariance of the observation noise
    R = matrix([[noise, 0], [0, noise]])

    # 5d identity matrix
    I = matrix([[]])
    I.identity(dim)

    # innovation or measurement residual
    y = matrix([[target_measurement[0]], [target_measurement[1]]]) - H * OTHER["x"]

    # innovation (or residual) covariance
    S = H * OTHER["P"] * H.transpose() + R

    # Near-optimal Kalman gain
    K = OTHER["P"] * H.transpose() * S.inverse()

    # updated state estimate
    OTHER["x"] = OTHER["x"] + K * y

    # updated covariance estimate
    OTHER["P"] = (I - K * H) * OTHER["P"]

    est_x = OTHER["x"].value[0][0]
    est_y = OTHER["x"].value[1][0]
    est_dir = OTHER["x"].value[2][0]
    est_turn = OTHER["x"].value[3][0]
    est_dist = OTHER["x"].value[4][0]
    est_angle = est_turn + est_dir

    # state transition matrix using current predicted states and Jacobians.
    F = matrix([
        [1, 0, -est_dist * sin(est_angle), -est_dist * sin(est_angle), cos(est_angle)],
        [0, 1, est_dist * cos(est_angle), est_dist * cos(est_angle), sin(est_angle)],
        [0, 0, 1, 1, 0],
        [0, 0, 0, 1, 0],
        [0, 0, 0, 0, 1]
    ])

    # update estimated position of target
    OTHER["x"] = matrix([
        [est_x + est_dist * cos(est_angle)],
        [est_y + est_dist * sin(est_angle)],
        [est_angle],
        [est_turn],
        [est_dist]
    ])

    # predicted covariance estimate
    OTHER["P"] = F * OTHER["P"] * F.transpose()

    OTHER["history"].append((OTHER["x"].value[0][0], OTHER["x"].value[1][0]))
    OTHER["num_steps"] += 1

    return OTHER

def angle_trunc(a):
    """This maps all angles to a domain of [-pi, pi]"""
    while a < 0.0:
        a += pi * 2
    return ((a + pi) % (pi * 2)) - pi


def get_heading(hunter_position, target_position):
    """Returns the angle, in radians, between the target and hunter positions"""
    hunter_x, hunter_y = hunter_position
    target_x, target_y = target_position
    heading = atan2(target_y - hunter_y, target_x - hunter_x)
    heading = angle_trunc(heading)
    return heading

def distance_between(point1, point2):
    """Computes distance between point1 and point2. Points are (x, y) pairs."""
    x1, y1 = point1
    x2, y2 = point2
    return sqrt((x2 - x1) ** 2 + (y2 - y1) ** 2)

def demo_grading(hunter_bot, target_bot, next_move_fcn, OTHER = None):
    """Returns True if your next_move_fcn successfully guides the hunter_bot
    to the target_bot. This function is here to help you understand how we
    will grade your submission."""
    max_distance = 0.98 * target_bot.distance # 0.98 is an example. It will change.
    separation_tolerance = 0.02 * target_bot.distance # hunter must be within 0.02 step size to catch target
    caught = False
    ctr = 0
    #For Visualization
    import turtle
    window = turtle.Screen()
    window.bgcolor('white')
    chaser_robot = turtle.Turtle()
    chaser_robot.shape('arrow')
    chaser_robot.color('blue')
    chaser_robot.resizemode('user')
    chaser_robot.shapesize(0.3, 0.3, 0.3)
    broken_robot = turtle.Turtle()
    broken_robot.shape('turtle')
    broken_robot.color('green')
    broken_robot.resizemode('user')
    broken_robot.shapesize(0.3, 0.3, 0.3)
    size_multiplier = 15.0 #change size of animation
    chaser_robot.hideturtle()
    chaser_robot.penup()
    chaser_robot.goto(hunter_bot.x*size_multiplier, hunter_bot.y*size_multiplier-100)
    chaser_robot.showturtle()
    broken_robot.hideturtle()
    broken_robot.penup()
    broken_robot.goto(target_bot.x*size_multiplier, target_bot.y*size_multiplier-100)
    broken_robot.showturtle()
    measuredbroken_robot = turtle.Turtle()
    measuredbroken_robot.shape('circle')
    measuredbroken_robot.color('red')
    measuredbroken_robot.penup()
    measuredbroken_robot.resizemode('user')
    measuredbroken_robot.shapesize(0.1, 0.1, 0.1)
    broken_robot.pendown()
    chaser_robot.pendown()
    #End of Visualization
    # We will use your next_move_fcn until we catch the target or time expires.
    while not caught and ctr < 1000:
        # Check to see if the hunter has caught the target.
        hunter_position = (hunter_bot.x, hunter_bot.y)
        target_position = (target_bot.x, target_bot.y)
        separation = distance_between(hunter_position, target_position)
        if separation < separation_tolerance:
            print "You got it right! It took you ", ctr, " steps to catch the target."
            caught = True

        # The target broadcasts its noisy measurement
        target_measurement = target_bot.sense()

        # This is where YOUR function will be called.
        turning, distance, OTHER = next_move_fcn(hunter_position, hunter_bot.heading, target_measurement, max_distance, OTHER)

        # Don't try to move faster than allowed!
        if distance > max_distance:
            distance = max_distance

        # We move the hunter according to your instructions
        hunter_bot.move(turning, distance)

        # The target continues its (nearly) circular motion.
        target_bot.move_in_polygon()
        #Visualize it
        measuredbroken_robot.setheading(target_bot.heading*180/pi)
        measuredbroken_robot.goto(target_measurement[0]*size_multiplier, target_measurement[1]*size_multiplier-100)
        measuredbroken_robot.stamp()
        broken_robot.setheading(target_bot.heading*180/pi)
        broken_robot.goto(target_bot.x*size_multiplier, target_bot.y*size_multiplier-100)
        chaser_robot.setheading(hunter_bot.heading*180/pi)
        chaser_robot.goto(hunter_bot.x*size_multiplier, hunter_bot.y*size_multiplier-100)
        #End of visualization
        ctr += 1
        if ctr >= 1000:
            print "It took too many steps to catch the target."
    return caught

'''target = robot(0.0, 10.0, 0.0, 2*pi / 30, 1.5)
measurement_noise = .05*target.distance
target.set_noise(0.0, 0.0, measurement_noise)

hunter = robot(-10.0, -10.0, 0.0)

print demo_grading(hunter, target, next_move)
'''