# These import steps give you access to libraries which you may (or may
# not) want to use.
from math import *
from robot import *
from matrix import *
import random

# This is the function you have to write. The argument 'measurement' is a 
# single (x, y) point. This function will have to be called multiple
# times before you have enough information to accurately predict the
# next position. The OTHER variable that your function returns will be 
# passed back to your function the next time it is called. You can use
# this to keep track of important information over time.
def estimate_next_pos(measurement, OTHER=None):
    """Estimate the next (x, y) position of the wandering Traxbot
    based on noisy (x, y) measurements."""

    # OTHER holds previously known (x,y) coordinate and heading
    if OTHER is None:
        prev_angle = 0.0
        OTHER = dict()
        OTHER["prev_measurement"] = measurement
        OTHER["prev_angle"] = prev_angle
        return (0.0, 0.0), OTHER

    prev_measurement = OTHER["prev_measurement"]
    prev_predicted_angle = OTHER["prev_angle"]

    distance_result = distance_between(measurement, prev_measurement)

    # calculate difference in past and current measurements
    # to get predicted angle
    predicted_angle = atan2(measurement[1] - prev_measurement[1],
                            measurement[0] - prev_measurement[0])

    # persist current measurement and angle to next function call
    OTHER["prev_measurement"] = measurement
    OTHER["prev_angle"] = predicted_angle

    # based on previous and current angle, guess what new angle will be
    heading = predicted_angle + predicted_angle - prev_predicted_angle
    heading = angle_trunc(heading)

    # You must return xy_estimate (x, y), and OTHER (even if it is None)
    # in this order for grading purposes.
    return (measurement[0] + distance_result * cos(heading),
            measurement[1] + distance_result * sin(heading)), OTHER

# A helper function you may find useful.
def distance_between(point1, point2):
    """Computes distance between point1 and point2. Points are (x, y) pairs."""
    x1, y1 = point1
    x2, y2 = point2
    return sqrt((x2 - x1) ** 2 + (y2 - y1) ** 2)
