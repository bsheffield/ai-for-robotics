# Optimize your PID parameters here:
pressure_tau_p = 0.1
pressure_tau_d = 1.0

rocket_tau_p = 7.
rocket_tau_i = 0.2
rocket_tau_d = .5

def pressure_pd_solution(delta_t, current_pressure, data):
    """Student solution to maintain LOX pressure to the turbopump at a level of 100.

    Args:
        delta_t (float): Time step length.
        current_pressure (float): Current pressure level of the turbopump.
        data (dict): Data passed through out run.  Additional data can be added and existing values modified.
            'ErrorP': Proportional error.  Initialized to 0.0
            'ErrorD': Derivative error.  Initialized to 0.0
    """

    #naive solution
    #adjust_pressure = current_pressure

    #PD Controller formula assuming delta_t=1
    #alpha = -tau_p*CTE - tau_d(CTE_t - CTE_t-1)

    #approach max_pump_pressure at a rate of max_pressure_increase until tolerance is reached.
    tolerance = 95
    max_pressure_increase = 10
    max_pump_pressure = 100
    if current_pressure < tolerance:
        adjust_pressure = max_pressure_increase
        data['prev_cte'] = current_pressure - max_pump_pressure
        return adjust_pressure, data

    #Once tolerance in pressure has been reached, calculate crosstrack and optimize
    # pressure with PD controller.
    cte = current_pressure - max_pump_pressure
    diff_cte = cte - data['prev_cte']
    data['prev_cte'] = cte
    adjust_pressure = -pressure_tau_p * cte - pressure_tau_d * diff_cte

    return adjust_pressure, data

def rocket_pid_solution(delta_t, current_velocity, optimal_velocity, data):
    """Student solution for maintaining rocket throttle through out the launch based on an optimal flight path

    Args:
        delta_t (float): Time step length.
        current_velocity (float): Current velocity of rocket.
        optimal_velocity (float): Optimal velocity of rocket.
        data (dict): Data passed through out run.  Additional data can be added and existing values modified.
            'ErrorP': Proportional error.  Initialized to 0.0
            'ErrorI': Integral error.  Initialized to 0.0
            'ErrorD': Derivative error.  Initialized to 0.0

    Returns:
        Throttle to set, data dictionary to be passed through run.
    """

    #remove naive solution
    #throttle = optimal_velocity - current_velocity
    #print("Optimal Velocity: " + str(optimal_velocity))

    landing = -0.5
    if optimal_velocity <= landing:
        return 0, data

    cte = current_velocity - optimal_velocity

    if not data.has_key('cte'):
        data['cte'] = list()
    #print("Current: " + str(current_velocity) + " Optimal: " + \
    # str(optimal_velocity) + " CTE: " + str(cte))

    #if list is empty, then add 0 so we can pop off in next operation.
    if len(data['cte']) < 1:
        data['cte'].append(0)

    diff_cte = cte - data['cte'][-1]
    data['cte'].append(cte)

    #PID Controller algorithm assuming delta_t=1: #alpha = -tau_p*CTE - tau_d(CTE_t - CTE_t-1)
    throttle = -rocket_tau_p * cte - rocket_tau_d * diff_cte - rocket_tau_i * sum(data['cte'])
    #print("Throttle: " + str(throttle))

    #Throttle control in case values exceed 0 lower bound and 1 upper bound.
    if throttle > 1:
        return 1, data
    elif throttle < 0:
        return 0, data
    else:
        return throttle, data
