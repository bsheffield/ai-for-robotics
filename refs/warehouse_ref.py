#
# === Introduction ===
#
# In this problem, you will build a planner that helps a robot
#   find the best path through a warehouse filled with boxes
#   that it has to pick up and deliver to a dropzone.
#
# Your file must be called `partA.py` and must have a class
#   called `DeliveryPlanner`.
# This class must have an `__init__` function that takes three
#   arguments: `self`, `warehouse`, and `todo`.
# The class must also have a function called `plan_delivery` that
#   takes a single argument, `self`.
#
# === Input Specifications ===
#
# `warehouse` will be a list of m strings, each with n characters,
#   corresponding to the layout of the warehouse. The warehouse is an
#   m x n grid. warehouse[i][j] corresponds to the spot in the ith row
#   and jth column of the warehouse, where the 0th row is the northern
#   end of the warehouse and the 0th column is the western end.
#
# The characters in each string will be one of the following:
#
# '.' (period) : traversable space. The robot may enter from any adjacent space.
# '#' (hash) : a wall. The robot cannot enter this space.
# '@' (dropzone): the starting point for the robot and the space where all boxes must be delivered.
#   The dropzone may be traversed like a '.' space.
# [0-9a-zA-Z] (any alphanumeric character) : a box. At most one of each alphanumeric character
#   will be present in the warehouse (meaning there will be at most 62 boxes). A box may not
#   be traversed, but if the robot is adjacent to the box, the robot can pick up the box.
#   Once the box has been removed, the space functions as a '.' space.
#
# For example,
#   warehouse = ['1#2',
#                '.#.',
#                '..@']
#   is a 3x3 warehouse.
#   - The dropzone is at the warehouse cell in row 2, column 2.
#   - Box '1' is located in the warehouse cell in row 0, column 0.
#   - Box '2' is located in the warehouse cell in row 0, column 2.
#   - There are walls in the warehouse cells in row 0, column 1 and row 1, column 1.
#   - The remaining five warehouse cells contain empty space.
#
# The argument `todo` is a list of alphanumeric characters giving the order in which the
#   boxes must be delivered to the dropzone. For example, if
#   todo = ['1','2']
#   is given with the above example `warehouse`, then the robot must first deliver box '1'
#   to the dropzone, and then the robot must deliver box '2' to the dropzone.
#
# === Rules for Movement ===
#
# - Two spaces are considered adjacent if they share an edge or a corner.
# - The robot may move horizontally or vertically at a cost of 2 per move.
# - The robot may move diagonally at a cost of 3 per move.
# - The robot may not move outside the warehouse.
# - The warehouse does not "wrap" around.
# - As described earlier, the robot may pick up a box that is in an adjacent square.
# - The cost to pick up a box is 4, regardless of the direction the box is relative to the robot.
# - While holding a box, the robot may not pick up another box.
# - The robot may put a box down on an adjacent empty space ('.') or the dropzone ('@') at a cost
#   of 2 (regardless of the direction in which the robot puts down the box).
# - If a box is placed on the '@' space, it is considered delivered and is removed from the ware-
#   house.
# - The warehouse will be arranged so that it is always possible for the robot to move to the
#   next box on the todo list without having to rearrange any other boxes.
#
# An illegal move will incur a cost of 100, and the robot will not move (the standard costs for a
#   move will not be additionally incurred). Illegal moves include:
# - attempting to move to a nonadjacent, nonexistent, or occupied space
# - attempting to pick up a nonadjacent or nonexistent box
# - attempting to pick up a box while holding one already
# - attempting to put down a box on a nonadjacent, nonexistent, or occupied space
# - attempting to put down a box while not holding one
#
# === Output Specifications ===
#
# `plan_delivery` should return a LIST of moves that minimizes the total cost of completing
#   the task successfully.
# Each move should be a string formatted as follows:
#
# 'move {i} {j}', where '{i}' is replaced by the row-coordinate of the space the robot moves
#   to and '{j}' is replaced by the column-coordinate of the space the robot moves to
#
# 'lift {x}', where '{x}' is replaced by the alphanumeric character of the box being picked up
#
# 'down {i} {j}', where '{i}' is replaced by the row-coordinate of the space the robot puts
#   the box, and '{j}' is replaced by the column-coordinate of the space the robot puts the box
#
# For example, for the values of `warehouse` and `todo` given previously (reproduced below):
#   warehouse = ['1#2',
#                '.#.',
#                '..@']
#   todo = ['1','2']
# `plan_delivery` might return the following:
#   ['move 2 1',
#    'move 1 0',
#    'lift 1',
#    'move 2 1',
#    'down 2 2',
#    'move 1 2',
#    'lift 2',
#    'down 2 2']
#
# === Grading ===
#
# - Your planner will be graded against a set of test cases, each equally weighted.
# - If your planner returns a list of moves of total cost that is K times the minimum cost of
#   successfully completing the task, you will receive 1/K of the credit for that test case.
# - Otherwise, you will receive no credit for that test case. This could happen for one of several
#   reasons including (but not necessarily limited to):
#   - plan_delivery's moves do not deliver the boxes in the correct order.
#   - plan_delivery's output is not a list of strings in the prescribed format.
#   - plan_delivery does not return an output within the prescribed time limit.
#   - Your code raises an exception.
#
# === Additional Info ===
#
# - You may add additional classes and functions as needed provided they are all in the file `partA.py`.
# - Upload partA.py to Project 2 on T-Square in the Assignments section. Do not put it into an
#   archive with other files.
# - Your partA.py file must not execute any code when imported.
# - Ask any questions about the directions or specifications on Piazza.
#
import math
import random

PI = math.pi


def compute_distance(p, q):
    x1, y1 = p
    x2, y2 = q

    dx = x2 - x1
    dy = y2 - y1

    return math.sqrt(dx ** 2 + dy ** 2)


def compute_bearing(p, q):
    x1, y1 = p
    x2, y2 = q

    dx = x2 - x1
    dy = y2 - y1

    return math.atan2(dy, dx)


def truncate_angle(t):
    return ((t + PI) % (2 * PI)) - PI


class Robot:
    def __init__(self, x=0.0, y=0.0, bearing=0.0, max_distance=1.0, max_steering=PI / 4):
        self.x = x
        self.y = y
        self.bearing = bearing
        self.max_distance = max_distance
        self.max_steering = max_steering

    def set_noise(self, steering_noise, distance_noise, measurement_noise):
        self.steering_noise = float(steering_noise)
        self.distance_noise = float(distance_noise)
        self.measurement_noise = float(measurement_noise)

    # move the robot
    def move(self, steering, distance, noise=False):
        if noise:
            steering += random.uniform(-0.01, 0.01)
            distance *= random.uniform(0.99, 1.01)

        steering = max(-self.max_steering, steering)
        steering = min(self.max_steering, steering)
        distance = max(0, distance)
        distance = min(self.max_distance, distance)

        self.bearing = truncate_angle(self.bearing + float(steering))
        self.x += distance * math.cos(self.bearing)
        self.y += distance * math.sin(self.bearing)

    def measure_distance_and_bearing_to(self, point, noise=False):

        current_position = (self.x, self.y)

        distance_to_point = compute_distance(current_position, point)
        bearing_to_point = compute_bearing(current_position, point)

        if noise:
            distance_sigma = 0.05 * distance_to_point
            bearing_sigma = 0.02 * distance_to_point

            distance_noise = random.gauss(0, distance_sigma)
            bearing_noise = random.gauss(0, bearing_sigma)
        else:
            distance_noise = 0
            bearing_noise = 0

        measured_distance = distance_to_point + distance_noise
        measured_bearing = truncate_angle(bearing_to_point - self.bearing + bearing_noise)

        return measured_distance, measured_bearing

    # find the next point without updating the robot's position
    def find_next_point(self, steering, distance):

        steering = max(-self.max_steering, steering)
        steering = min(self.max_steering, steering)
        distance = max(0, distance)
        distance = min(self.max_distance, distance)

        bearing = truncate_angle(self.bearing + float(steering))
        x = self.x + (distance * math.cos(bearing))
        y = self.y + (distance * math.sin(bearing))

        return x, y

    def __repr__(self):
        """This allows us to print a robot's position"""
        return '[%.5f, %.5f]' % (self.x, self.y)


# ######################
delta = [[-1, 0], [0, -1], [1, 0], [0, 1], [-1, -1], [1, 1], [-1, 1], [1, -1]]
cost = [2, 2, 2, 2, 3, 3, 3, 3]
lift_cost = 4
down_cost = 2
ACTIONS = {}
delta_name = ['^', '<', 'v', '>', 'NW', 'SE', 'NE', 'SW']


# move_list = plan_delivery(warehouse_state, dropzone, boxes, todo)
# for row in move_list:
#     print (row)

class DeliveryPlanner:

    # find the dropzone and make a list of box locations
    def get_data(self,warehouse):
        rows = len(warehouse)
        cols = len(warehouse[0])
        warehouse_state = [[0 for col in range(len(warehouse[0]))] for row in range(len(warehouse))]
        boxes = {}
        for i in range(rows):
            for j in range(cols):

                this_square = warehouse[i][j]

                if this_square == '.':
                    warehouse_state[i][j] = '.'

                elif this_square == '#':
                    warehouse_state[i][j] = '#'

                elif this_square == '@':
                    warehouse_state[i][j] = '@'
                    dropzone = (i, j)

                else:  # a box
                    box_id = this_square
                    warehouse_state[i][j] = box_id
                    boxes[box_id] = (i, j)
        return (warehouse_state, dropzone, boxes)

        warehouse_state, dropzone, boxes = get_data(warehouse)
        print('warehouse state')
        for row in warehouse_state:
            print(row)
        print('dropzone', dropzone)
        print('boxes', boxes)
        print('==============')

    # ######################
    # Find a list of possible goals
    def find_poss_goals(self, ult_goal, warehouse_state):
        rows = len(warehouse_state)
        cols = len(warehouse_state[0])
        x = ult_goal[0]
        y = ult_goal[1]

        possible_goals = []

        for i in range(len(delta)):
            x1 = x + delta[i][0]
            y1 = y + delta[i][1]
            if x1 >= 0 and x1 < rows and y1 >= 0 and y1 < cols:
                if warehouse_state[x1][y1] != '#' and not str.isalnum(warehouse_state[x1][y1]):
                    possible_goals.append([x1, y1])
        return possible_goals

    # ######################
    # optimum_policy function is from Udacity - AI4R Optimum Policy
    def optimum_policy(self,grid, goal, cost):
        rows = len(grid)
        cols = len(grid[0])
        value = [[99 for col in range(cols)] for row in range(rows)]
        policy = [[' ' for col in range(cols)] for row in range(rows)]

        change = True
        while change is True:
            change = False
            for y in range(cols):
                for x in range(rows):
                    if goal[0] == x and goal[1] == y:
                        if value[x][y] > 0:
                            value[x][y] = 0
                            policy[x][y] = '*'
                            change = True

                    elif warehouse_state[x][y] == '.':
                        for i in range(len(delta)):
                            x2 = x + delta[i][0]
                            y2 = y + delta[i][1]
                            if x2 >= 0 and x2 < rows and y2 >= 0 and y2 < cols and warehouse_state[x2][y2] == '.':
                                v2 = value[x2][y2] + cost[i]
                                if v2 < value[x][y]:
                                    change = True
                                    value[x][y] = v2
                                    policy[x][y] = delta_name[i]
        return policy, value

    # output is min_goal out of possible goals and min_cost (one-way moves)
    def choose_goal(self,start, end, warehouse_state, cost):
        rows = len(warehouse_state)
        cols = len(warehouse_state[0])
        possible_goals = find_poss_goals(end, warehouse_state)
        # print('possible goals', possible_goals)
        min_cost = 200
        for j in range(len(possible_goals)):
            goal = possible_goals[j]
            # print('goal', goal)
            policy, value = optimum_policy(warehouse_state, goal, cost)
            x_init = start[0]
            y_init = start[1]
            if x_init == goal[0] and y_init == goal[1]:
                min_goal = start
                min_cost = 0
            else:
                for i in range(len(delta)):
                    x = x_init + delta[i][0]
                    y = y_init + delta[i][1]
                    if x >= 0 and x < rows and y >= 0 and y < cols and warehouse_state[x][y] == '.':
                        cost_test = (value[x][y])
                        if cost_test < min_cost:
                            min_cost = cost_test + cost[i]
                            min_goal = goal
                            first_step = [x, y]
        # print('minimum goal', min_goal, 'cost', min_cost)
        return min_goal, min_cost

    # output is first_step, key
    def find_first_step(self,init_space, min_goal, warehouse_state, cost):
        rows = len(warehouse_state)
        cols = len(warehouse_state[0])
        min_policy, value = optimum_policy(warehouse_state, min_goal, cost)
        # cost_compare = 99
        x_init = init_space[0]
        y_init = init_space[1]
        pnm = {}
        for i in range(len(delta)):
            x2 = x_init + delta[i][0]
            y2 = y_init + delta[i][1]
            if x2 >= 0 and x2 < rows and y2 >= 0 and y2 < cols and warehouse_state[x2][y2] == '.':
                # print(value[x2][y2])
                pnm[value[x2][y2]] = x2, y2
        comp = 100
        for key, first_step in pnm.iteritems():
            if key < comp:
                comp = key
                # print(first_step, key)
        return first_step, key

    # create a function here which finds the rest of the path given a position
    # returns move_list, last_place
    def follow_policy(self,start, min_policy, move_list):
        x_init = start[0]
        y_init = start[1]
        while min_policy[x_init][y_init] != '*':
            move = min_policy[x_init][y_init]
            position = delta_name.index(move)
            next_move = delta[position]
            # print('nextmove', next_move)
            x_init = x_init + next_move[0]
            y_init = y_init + next_move[1]
            f = 'move ' + str(x_init) + ' ' + str(y_init)
            move_list.append(f)
            last_place = x_init, y_init
        else:
            last_place = x_init, y_init

        return move_list, last_place

    def plan_delivery(self,warehouse_state, dropzone, boxes, todo):
        move_list = []
        for k in range(len(todo)):
            box_no = todo[k]
            # print(box_no)
            ultgoal = boxes[box_no]
            # print(boxes[box_no])

            if k == 0:
                # Find the best position to lift the first box
                min_goal, min_cost = choose_goal(dropzone, ultgoal, warehouse_state, cost)

                # find the optimum_policy for that position
                min_policy, value = optimum_policy(warehouse_state, min_goal, cost)

                # Find the first step while on the dropzone
                first_step, key = find_first_step(dropzone, min_goal, warehouse_state, cost)
                # print ('step 1', first_step)
                f = 'move ' + str(first_step[0]) + ' ' + str(first_step[1])
                move_list.append(f)
                # after finding the first step, follow the path (here to the first box)
                move_list, last_place = follow_policy(first_step, min_policy, move_list)

                # pick up the box
                lift_move = 'lift ' + str(box_no)
                move_list.append(lift_move)

                # Find the best position next to the dropzone
                min_goal, min_cost = choose_goal(last_place, dropzone, warehouse_state, cost)

                # find the optimum_policy next to the dropzone
                min_policy, value = optimum_policy(warehouse_state, min_goal, cost)

                move_list, last_place = follow_policy(last_place, min_policy, move_list)
                # drop the box
                drop_move = 'down' + str(dropzone[0]) + ' ' + str(dropzone[1])
                move_list.append(drop_move)

                # print('=============')

            else:
                box_no = todo[k]
                ultgoal = boxes[box_no]

                # Find the best position to lift the second box
                min_goal, min_cost = choose_goal(last_place, ultgoal, warehouse_state, cost)
                # find the optimum_policy for that position
                min_policy, value = optimum_policy(warehouse_state, min_goal, cost)

                # after finding the first step, if follow the path (here to the box)
                move_list, last_place = follow_policy(last_place, min_policy, move_list)
                # pick up the box
                lift_move = 'lift ' + str(box_no)
                move_list.append(lift_move)

                # find the best position next to the dropzone
                min_goal, min_cost = choose_goal(last_place, dropzone, warehouse_state, cost)

                # find the optimum_policy for that position
                min_policy, value = optimum_policy(warehouse_state, min_goal, cost)
                move_list, last_place = follow_policy(last_place, min_policy, move_list)
                # drop the box
                drop_move = 'down ' + str(dropzone[0]) + ' ' + str(dropzone[1])
                move_list.append(drop_move)
                # print('=============')

        return move_list