
class DeliveryPlanner:

    def __init__(self, warehouse, todo):
        self.warehouse = warehouse
        self.dropzone = list()
        self.boxes = list()
        self.set_all_objectives(todo)
        self.delta = [[-1, 0],  #UP
             [0, -1],  #LEFT
             [1, 0],  #DOWN
             [0, 1],  #RIGHT
             [-1, -1],  #UP-LEFT
             [1, -1],  #DOWN-LEFT
             [1, 1],  #DOWN-RIGHT
            [-1,1]] # UP-RIGHT
        self.cost_deltas = [2, 2, 2, 2, 3, 3, 3, 3]

    def set_all_objectives(self, todo):

        dz_symbol = "@"
        self.boxes = [[j, k] for i in range(len(todo))
                      for j in range(len(self.warehouse))
                      for k in range(len(self.warehouse[j]))
                      if self.warehouse[j][k] == todo[i]]
        dz = [[i, j] for i in range(len(self.warehouse))
                for j in range(len(self.warehouse[i]))
                if self.warehouse[i][j] == dz_symbol]
        self.dropzone = dz[0]

    def compute_value(self, start):

        value = [[99 for row in range(len(self.warehouse[0]))] for col in range(len(self.warehouse))]
        action = [[0 for row in range(len(self.warehouse[0]))] for col in range(len(self.warehouse))]
        value[start[0]][start[1]] = 0
        change = True

        while change:
            change = False

            for x in range(len(self.warehouse)):
                for y in range(len(self.warehouse[0])):
                    if [x, y] != start:

                        for a in range(len(self.delta)):
                            x2 = x + self.delta[a][0]
                            y2 = y + self.delta[a][1]

                            if x2 >= 0 and x2 < len(self.warehouse) \
                                    and y2 >= 0 and y2 < len(self.warehouse[0]) and \
                                    (self.warehouse[x2][y2] == '.' or [x2, y2] == start or [x2, y2] == self.dropzone):

                                v2 = value[x2][y2] + self.cost_deltas[a]
                                if v2 < value[x][y]:
                                    change = True
                                    value[x][y] = v2
                                    action[x][y] = a
        print("Value: " + str(value))
        print("Action: " + str(action))
        return value, action

    def find_point(self, target, value):
        temp = list()
        for a in range(len(self.delta)):
            x2 = target[0] + self.delta[a][0]
            y2 = target[1] + self.delta[a][1]

            if x2 >= 0 and x2 < len(self.warehouse) and y2 >= 0 \
                    and y2 < len(self.warehouse[0]) \
                    and (self.warehouse[x2][y2] == '.'
                         or self.warehouse[x2][y2] == '@'):
                temp.append([value[x2][y2], x2, y2])

        temp.sort()
        temp.reverse()
        point = temp.pop()
        return [point[1], point[2]]

    def find_path(self, start, end, action):
        path = list()
        [x, y] = start
        path.append([x, y])

        while [x, y] != end:
            x2 = x + self.delta[action[x][y]][0]
            y2 = y + self.delta[action[x][y]][1]
            x = x2
            y = y2
            path.append([x, y])

        path.pop()
        path.reverse()
        return path

    def plan_delivery(self):
        cost = 0
        lift_cost = 4
        down_cost = 2
        moves = list()
        box_num = 1
        all_moves = list()
        start = self.dropzone
        value, action = self.compute_value(start)
        h = [[0 for row in range(len(self.warehouse[0]))] for col in range(len(self.warehouse))]
        result = self.search(self.warehouse,start,self.boxes[0],self.cost_deltas,h)

        for i in range(len(self.boxes)):
            end = self.find_point(self.boxes[i], value)
            m, n = end
            path_go = self.find_path(end, start, action)
            path_go.append('lift')
            cost = cost + value[m][n] + lift_cost
            self.warehouse[self.boxes[i][0]] = str(self.warehouse[self.boxes[i][0]][:self.boxes[i][1]]) \
                                               + '.' + str(self.warehouse[self.boxes[i][0]][self.boxes[i][1] + 1:])
            start = end
            value, action = self.compute_value(start)
            end = self.find_point(self.dropzone, value)
            m, n = end
            path_return = self.find_path(end, start, action)
            path_return.append('down')
            cost = cost + value[m][n] + down_cost
            path_go.extend(path_return)
            all_moves.extend(path_go)
            start = end
            value, action = self.compute_value(start)

        for i in range(len(all_moves)):
            if all_moves[i] == 'lift':
                moves.append('lift ' + str(box_num))
                box_num += 1
            elif all_moves[i] == 'down':
                moves.append('down ' + str(self.dropzone[0]) + ' ' + str(self.dropzone[1]))
            else:
                moves.append('move ' + str(all_moves[i][0]) + ' ' + str(all_moves[i][1]))
        return moves

    def search(self, grid, init, goal, cost, heuristic):

        closed = [[0 for row in range(len(grid[0]))] for col in range(len(grid))]
        closed[init[0]][init[1]] = 1

        expand = [[-1 for row in range(len(grid[0]))] for col in range(len(grid))]
        actions = [[-1 for row in range(len(grid[0]))] for col in range(len(grid))]


        x = init[0]
        y = init[1]
        g = 0
        h = heuristic[x][y]
        f = g + h

        # expand elements in the open list.
        # h is not necessary.
        # f is the first element because it is used in sorting.
        open = [[f, g, h, x, y]]

        found = False  # flag that is set when search is complete
        resign = False  # flag set if we can't find expand
        count = 0

        while not found and not resign:
            if len(open) == 0:
                resign = True
                print("Fail")
                return "Fail"
            else:
                open.sort()
                open.reverse()
                next = open.pop()

                x = next[3]
                y = next[4]
                g = next[1]
                expand[x][y] = count
                count += 1

                #if x == goal[0] and y == goal[1]:
                if abs(x - goal[0]) + abs(y - goal[1]) <= 1:
                    found = True
                else:
                    # expand winning element and add to new open list
                    for i in range(len(self.delta)):
                        x2 = x + self.delta[i][0]
                        y2 = y + self.delta[i][1]
                        if x2 >= 0 and x2 < len(grid) and y2 >= 0 and y2 < len(grid[0]):
                            if closed[x2][y2] == 0 and grid[x2][y2] in [".", "@"]:#grid[x2][y2] == 0:
                                g2 = g + cost[i]
                                h2 = heuristic[x2][y2]
                                f2 = g2 + h2
                                open.append([f2, g2, h2, x2, y2])
                                closed[x2][y2] = 1
                                actions[x2][y2] = i
        print("Expand: ")
        for e in expand:
            print(e)
        print("Action: ")
        for a in actions:
            print(a)
        #print(actions)
        #print(expand)
        return expand


warehouse = ['1#2',
             '.#.',
             '..@']
todo = ['1', '2']
dp = DeliveryPlanner(warehouse, todo)
dp.plan_delivery()
