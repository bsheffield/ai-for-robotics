
import math
import random
import copy

PI = math.pi
factor = 5

def compute_distance(p, q):
    x1, y1 = p
    x2, y2 = q

    dx = x2 - x1
    dy = y2 - y1

    return math.sqrt(dx ** 2 + dy ** 2)

def compute_bearing(p, q):
    x1, y1 = p
    x2, y2 = q

    dx = x2 - x1
    dy = y2 - y1

    return math.atan2(dy, dx)


def truncate_angle(t):
    return ((t + PI) % (2 * PI)) - PI


class Robot:
    def __init__(self, x=0.0, y=0.0, bearing=0.0, max_distance=1.0, max_steering=PI / 4):
        self.x = x
        self.y = y
        self.bearing = bearing
        self.max_distance = max_distance
        self.max_steering = max_steering

    def set_noise(self, steering_noise, distance_noise, measurement_noise):
        self.steering_noise = float(steering_noise)
        self.distance_noise = float(distance_noise)
        self.measurement_noise = float(measurement_noise)

    # move the robot
    def move(self, steering, distance, noise=False):
        if noise:
            steering += random.uniform(-0.01, 0.01)
            distance *= random.uniform(0.99, 1.01)

        steering = max(-self.max_steering, steering)
        steering = min(self.max_steering, steering)
        distance = max(0, distance)
        distance = min(self.max_distance, distance)

        self.bearing = truncate_angle(self.bearing + float(steering))
        self.x += distance * math.cos(self.bearing)
        self.y += distance * math.sin(self.bearing)

    def measure_distance_and_bearing_to(self, point, noise=False):

        current_position = (self.x, self.y)

        distance_to_point = compute_distance(current_position, point)
        bearing_to_point = compute_bearing(current_position, point)

        if noise:
            distance_sigma = 0.05 * distance_to_point
            bearing_sigma = 0.02 * distance_to_point

            distance_noise = random.gauss(0, distance_sigma)
            bearing_noise = random.gauss(0, bearing_sigma)
        else:
            distance_noise = 0
            bearing_noise = 0

        measured_distance = distance_to_point + distance_noise
        measured_bearing = truncate_angle(bearing_to_point - self.bearing + bearing_noise)

        return measured_distance, measured_bearing

    # find the next point without updating the robot's position
    def find_next_point(self, steering, distance):

        steering = max(-self.max_steering, steering)
        steering = min(self.max_steering, steering)
        distance = max(0, distance)
        distance = min(self.max_distance, distance)

        bearing = truncate_angle(self.bearing + float(steering))
        x = self.x + (distance * math.cos(bearing))
        y = self.y + (distance * math.sin(bearing))

        return x, y

    def __repr__(self):
        """This allows us to print a robot's position"""
        return '[%.5f, %.5f]' % (self.x, self.y)


def expand_warehouse(warehouse, factor):
    expand = [[' ' for row in range(len(warehouse[0]) * factor)] for col in range(len(warehouse) * factor)]
    for i in range(len(expand)):
        for j in range(len(expand[0])):
            expand[i][j] = warehouse[i / factor][j / factor]
    return expand


def adding_wall_clearance(warehouse):
    w = len(warehouse[0])
    h = len(warehouse)
    new = copy.deepcopy(warehouse)  # [[' ' for row in range(len(warehouse[0]))] for col in range(len(warehouse))]
    for i in range(h):
        for j in range(w):
            if warehouse[i][j] == '#' and j - 1 >= 0 and j + 1 < w and i - 1 >= 0 and i + 1 < h:
                new[i][j - 1] = '#'
                new[i][j + 1] = '#'
                new[i - 1][j] = '#'
                new[i + 1][j] = '#'
                new[i - 1][j - 1] = '#'
                new[i + 1][j + 1] = '#'
                new[i - 1][j + 1] = '#'
                new[i + 1][j - 1] = '#'
    for j in range(w):
        new[0][j] = '#'
        new[-1][j] = '#'

    for i in range(h):
        new[i][0] = '#'
        new[i][-1] = '#'

    return new

def adding_box_clearance(warehouse, box):
    w = len(warehouse[0])
    h = len(warehouse)
    new = copy.deepcopy(warehouse)
    for i in range(h):
        for j in range(w):
            if (box[0] - (2 * j + 1) * 0.1) ** 2 + (box[1] + (2 * i + 1) * 0.1) ** 2 < 0.39 ** 2:
                new[i][j] = '#'
    return new

def clean_box(warehouse, box):
    w = len(warehouse[0])
    h = len(warehouse)
    new = copy.deepcopy(warehouse)
    for i in range(h):
        for j in range(w):
            if (box[0] - (2 * j + 1) * 0.1) ** 2 + (box[1] + (2 * i + 1) * 0.1) ** 2 < 0.39 ** 2:
                new[i][j] = '.'
    return new

def pos_calculation(all_path, max_steering, dropzone_coordinate):
    bearing = 0
    pos = list()
    for i in range(len(all_path) - 2):
        temp = list()
        if all_path[i] != 'lift' and all_path[i] != 'down':
            j = i + 1
            if all_path[j] == 'lift' or all_path[j] == 'down':
                j = i + 2
            temp.append(all_path[i])
            temp.append(bearing)
            steering = compute_bearing(all_path[i], all_path[j]) - bearing
            steering = truncate_angle(steering)

            if steering <= max_steering and steering >= -max_steering:
                temp.append(steering)
                pos.append(temp)
            else:

                if steering >= max_steering:
                    temp.append(max_steering)
                    pos.append(temp)
                    temp = list()
                    temp.append(all_path[i])
                    temp.append(bearing + max_steering)
                    temp.append(steering - max_steering)
                    pos.append(temp)
                    temp = list()
                if steering <= -max_steering:
                    temp.append(-max_steering)
                    pos.append(temp)
                    temp = list()
                    temp.append(all_path[i])
                    temp.append(bearing - max_steering)
                    temp.append(steering + max_steering)
                    pos.append(temp)
            bearing = compute_bearing(all_path[i], all_path[j])

        if all_path[i] == 'lift':
            pos.append('lift')
        if all_path[i] == 'down':
            pos.append('down')
    pos.append(copy.copy(pos[-1]))
    pos[-1][0] = dropzone_coordinate
    pos.append('down')
    return pos

def move_calculation(pos, factor, max_distance):
    moves = list()
    i = 0
    while i < len(pos) - 2:
        k = 0
        for j in range(i + 1, len(pos) - 1):

            if pos[j][0] == pos[i][0]:
                dist = compute_distance(pos[i][0], pos[j][0])
                temp = 'move ' + str(pos[i][2]) + ' ' + str(dist / factor)
                moves.append(temp)
                i = j
            elif pos[j][0] != pos[i][0] and pos[j][1] != pos[j + 1][1]:
                dist = compute_distance(pos[i][0], pos[j][0])
                temp = 'move ' + str(pos[i][2]) + ' ' + str(dist / factor)
                moves.append(temp)
                i = j
            elif pos[j][0] != pos[i][0] and pos[j][1] == pos[j + 1][1]:
                k = 1
        if k == 1:
            dist = compute_distance(pos[i][0], pos[j + 1][0])
            if dist >= max_distance * factor:
                temp = 'move ' + str(pos[i][2]) + ' ' + str((max_distance) / factor)
                moves.append(temp)
                temp = 'move ' + str(pos[i][2]) + ' ' + str((dist - max_distance) / factor)
                moves.append(temp)
            else:
                temp = 'move ' + str(pos[i][2]) + ' ' + str(dist / factor)
                moves.append(temp)
            i = j
    return moves

class DeliveryPlanner:

    def __init__(self, warehouse, todo, max_distance, max_steering):

        self.warehouse = copy.deepcopy(warehouse)
        self.todo = todo
        self.dropzone = self.find_dropzone(warehouse)
        self.todo_box = self.find_box(warehouse, todo)
        self.max_distance = max_distance
        self.max_steering = max_steering
        self.delta = [[-1, 0],  # go up
                      [0, -1],  # go left
                      [1, 0],  # go down
                      [0, 1],  # go right
                      [-1, -1],  # go up-left
                      [1, -1],  # go down-left
                      [1, 1],  # go down-right
                      [-1, 1]]  # go up-right

    def find_dropzone(self, warehouse):
        for i in range(len(self.warehouse)):
            for j in range(len(self.warehouse[0])):
                if self.warehouse[i][j] == '@':
                    return [i, j]

    def find_box(self, warehouse, todo):
        todo_box = list()
        for i in range(len(self.todo)):
            for j in range(len(self.warehouse)):
                for k in range(len(self.warehouse[0])):
                    if self.warehouse[j][k] == self.todo[i]:
                        todo_box.append([j, k])
        return todo_box

    def find_path(self, start, end):
        path = list()
        [x, y] = start
        path.append([y, -x])

        while [x, y] != end:
            x2 = x + self.delta[self.action[x][y]][0]
            y2 = y + self.delta[self.action[x][y]][1]
            x = x2
            y = y2
            path.append([y, -x])
        return path

    def compute_value(self, start):

        cost_step = [2.0, 2.0, 2.0, 2.0, 3.0, 3.0, 3.0, 3.0]
        self.value = [[100 for row in range(len(self.warehouse[0]))] for col in range(len(self.warehouse))]
        self.action = [[0 for row in range(len(self.warehouse[0]))] for col in range(len(self.warehouse))]
        self.value[start[0]][start[1]] = 0
        change = True

        while change:
            change = False
            for x in range(len(self.warehouse)):
                for y in range(len(self.warehouse[0])):
                    if [x, y] != start and self.warehouse[x][y] != '#':
                        for a in range(len(self.delta)):
                            x2 = x + self.delta[a][0]
                            y2 = y + self.delta[a][1]
                            if x2 >= 0 and x2 < len(self.warehouse) \
                                    and y2 >= 0 and y2 < len(self.warehouse[0]) and \
                                    (self.warehouse[x2][y2] == '.' or [x2, y2] == start or self.warehouse[x2][
                                        y2] == '@'):

                                v2 = self.value[x2][y2] + cost_step[a]
                                if v2 < self.value[x][y]:
                                    change = True
                                    self.value[x][y] = v2
                                    self.action[x][y] = a

        return self.value

    def plan_delivery(self):
        dropzone = self.find_dropzone(self.warehouse)
        dropzone = [dropzone[0] + 0.5, dropzone[1] + 0.5]
        self.warehouse = expand_warehouse(self.warehouse, factor)
        self.warehouse = adding_wall_clearance(self.warehouse)
        dropzone = [int(round(dropzone[0] * factor)) - 1, int(round(dropzone[1] * factor)) - 1]
        dropzone_coordinate = [dropzone[1], -dropzone[0]]

        for i in range(len(self.todo)):
            self.warehouse = adding_box_clearance(self.warehouse, self.todo[i])

        all_moves = list()
        box_coordinate = list()

        for i in range(len(self.todo)):
            self.warehouse = clean_box(self.warehouse, self.todo[i])
            value = self.compute_value(dropzone)
            end = self.todo[i]
            m = int(-round(end[1] * factor)) - 1
            n = int(round(end[0] * factor)) - 1
            end = [m, n]
            box_coordinate.append([n, -m])

            path_go = self.find_path(end, dropzone)
            path_go.reverse()
            path_go.pop()
            path_go.pop()  #
            path_go.append('lift')
            value = self.compute_value(end)  # update the cost map
            path_return = self.find_path(dropzone, end)
            path_return.pop()
            path_return.reverse()
            path_return.append('down')
            path_go.extend(path_return)
            all_moves.extend(path_go)

        pos = pos_calculation(all_moves, self.max_steering, dropzone_coordinate)
        seg_index = list()
        for i in range(len(pos)):
            if pos[i] == 'lift' or pos[i] == 'down':
                seg_index.append(i - 1)

        segment = list()
        start = 0
        for i in range(len(seg_index)):
            segment.append(pos[start:seg_index[i] + 1])
            start = seg_index[i]

        for each in segment:
            if 'lift' in each:
                each.remove('lift')
            if 'down' in each:
                each.remove('down')

        result = list()
        for i in range(len(segment)):
            result.append(move_calculation(segment[i], factor, self.max_distance))

        k = 0
        for i in range(len(result)):
            if i % 2 == 0:
                result[i].append('lift ' + str(k))
            else:
                result[i].append('down ' + str(dropzone[1] * 1.0 / factor) + ' ' + str(-dropzone[0] * 1.0 / factor))
                k = k + 1

        final_result = list()
        for i in range(len(result)):
            final_result = final_result + result[i]

        return final_result

