from matplotlib import pyplot as plt
from robot import Robot
from math import pi

# Citation: The map and plan syntax belong to the Georgia Tech CS8803-O01 Artificial
#           Intelligence for Robotics course assignment materials. The robot movement
#           API is copyright Udacity and is used throughout their Artificial Intelligence
#           for Robotics online lecture.


def _get_center(i, j):
    return j + 0.5, -i - 0.5


def _get_box_square(x, y):
    return [x-0.1, x+0.1, x+0.1, x-0.1], [y-0.1, y-0.1, y+0.1, y+0.1]


def plot(type, world, plan, todo=None):
    plt.xkcd(scale=1, length=200, randomness=1)

    world_height = len(world)
    world_width = len(world[0])
    plt.figure(figsize=(world_width * 1.5, world_height * 1.5))
    plt.gca().xaxis.tick_top()
    plt.xlim((0, world_width))
    plt.ylim((-world_height, 0))

    # Draw grid
    for x in range(world_width+1):
        plt.axvline(x=x, c='grey')
    for y in range(-world_height-1, 0):
        plt.axhline(y=y, c='grey')

    # Draw features
    dropzone_x, dropzone_y = None, None
    for i in range(world_height):
        for j in range(world_width):
            char = world[i][j]
            x_points = [j, j+1, j+1, j]
            y_points = [-i, -i, -i-1, -i-1]
            if char == '#':
                plt.fill(x_points, y_points, 'black')
            elif char == '@':
                plt.fill(x_points, y_points, 'C1')
                dropzone_x, dropzone_y = _get_center(i, j)
            elif char.isalnum():
                x, y = _get_center(i, j)
                plt.fill(*_get_box_square(x, y), c='C0', alpha=0.5)
                plt.annotate(char, (x, y), size=20)

    # Draw boxes
    if todo:
        for i, box in enumerate(todo):
            x, y = box
            plt.fill(*_get_box_square(x, y), c='C0', alpha=0.5)
            plt.annotate(i, (x, y), size=20)

    # Draw plan
    bot = Robot(dropzone_x, dropzone_y, 0, 100000., 2*pi)  # no movement limitations
    segment_start_x, segment_start_y = dropzone_x, dropzone_y
    carrying = False
    for action in plan:
        action_split = action.split()
        if action_split[0] == 'lift':
            carrying = True
        elif action_split[0] == 'down':
            carrying = False
            if type == 'PartA':
                down_x, down_y = _get_center(int(action_split[1]), int(action_split[2]))
            else:
                down_x, down_y = float(action_split[1]), float(action_split[2])
            plt.fill(*_get_box_square(down_x, down_y), c='C0', alpha=0.3)
            plt.scatter(down_x, down_y, s=80, c='C0', alpha=0.9)
        elif action_split[0] == 'move':
            if type == 'PartB':
                segment_start_x, segment_start_y = bot.x, bot.y
                bot.move(float(action_split[1]), float(action_split[2]), 0)
                segment_end_x, segment_end_y = bot.x, bot.y
            else:
                segment_end_x, segment_end_y = _get_center(float(action_split[1]), float(action_split[2]))
            linewidth_points = 0.5 * 72 * 1.15  # Cite: https://stackoverflow.com/questions/19394505/matplotlib-expand-the-line-with-specified-width-in-data-unit
            plt.plot([segment_start_x, segment_end_x], [segment_start_y, segment_end_y],
                     c='C2', linewidth=linewidth_points, solid_capstyle="round")
            if carrying:
                plt.plot([segment_start_x, segment_end_x], [segment_start_y, segment_end_y],
                         c='C0', linewidth=10)
            if type == 'PartB':
                plt.gca().add_artist(plt.Circle((segment_end_x, segment_end_y), 0.5, color='black', alpha=0.1))
            if type == 'PartA':
                segment_start_x, segment_start_y = segment_end_x, segment_end_y

    plt.show()


def simple_test_a():
    warehouse = ['.....',
                 '.....',
                 '..#d.',
                 '..#..',
                 '.....',
                 '....@']
    plan = []
    plan.append("move {} {}".format(4, 3))
    plan.append("move {} {}".format(3, 3))
    plan.append("lift {}".format('d'))
    plan.append("move {} {}".format(2, 4))
    plan.append("move {} {}".format(1, 4))
    plan.append("move {} {}".format(0, 4))
    plan.append("move {} {}".format(0, 3))
    plan.append("move {} {}".format(0, 2))
    plan.append("move {} {}".format(0, 1))
    plan.append("move {} {}".format(1, 1))
    plan.append("move {} {}".format(2, 1))
    plan.append("down {} {}".format(2, 0))
    plan.append("move {} {}".format(3, 1))
    plan.append("move {} {}".format(4, 1))
    plan.append("move {} {}".format(4, 2))
    plot('PartA', warehouse, plan)


def simple_test_b():
    warehouse = ['..#..',
                 '.....',
                 '..#..',
                 '..#..',
                 '.....',
                 '....@']
    plan = []
    plan.append("move {} {}".format(pi/2, 4.0))
    plan.append("move {} {}".format(pi/2 + pi/32, 3.35))
    plan.append("lift {}".format(0))
    plan.append("move {} {}".format(pi/3, 2.0))
    plan.append("move {} {}".format(.5, pi/3))
    plan.append("down {} {}".format(0.4, -5.15))
    plan.append("move {} {}".format(pi/2 - pi/32, 2.0))
    todo = []
    todo.append((0.8, -1.5))
    todo.append((4.1, -3.0))
    plot('PartB', warehouse, plan, todo)


if __name__ == "__main__":
    simple_test_a()